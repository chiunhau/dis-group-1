# DIS Group 1

[Demo](https://dis-group-1.herokuapp.com)

## Project setup

### 1. Install Node.js environment

Download and install [Node.js (LTS)](https://nodejs.org/en/). Restart the terminal if it's opened. Type the followinng command to see if it works in your terminal.

```
node -v
// This should return the version of the Node.js you just installed.
```

### 2. Setup the project

Clone from bitbucket and run the npm install command.

```sh
git clone git@bitbucket.org:chiunhau/dis-group-1.git
cd dis-group-1
npm install
```

### 3. Start development server

```sh
npm run dev
```

Now go to `localhost:3000` you should see the site with live-reload enabled.

## How to contribute?

Before working on any new feature, make sure to sync your local `master` with remote, and create (checkout) a new feature branch from `master`:

```
git remote update
git checkout master
git pull
git checkout -b feature/YOUR_FEATURE_NAME
```

After finishing your new feature, push the branch to remote and create pull requests to `master`.

```
git add .
git commit -m "your commit message"
git push origin feature/YOUR_FEATURE_NAME
```

Finally, go to Bitbucket sidebar > Pull requests, select from (your feature branch) and to (`master`), enter necessary information about your feature, and create a pull request.

## Contributors

- Aimilia-Marina Liosi <aimilia-marina@idmaster.eu>
- Chiunhau You <chiun-hau@idmaster.eu>
- Joanna Joa <joanna@idmaster.eu>
