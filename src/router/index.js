import { createRouter, createWebHistory } from 'vue-router';

import FrontPage from '../views/FrontPage.vue';
import Login from '../views/Login.vue';
import ResetPassword from '../views/ResetPassword.vue';
import Dashboard from '../views/Dashboard.vue';
import Permissions from '../views/Permissions.vue';

import CustomersNew from '../views/CustomersNew.vue';
import CustomersList from '../views/CustomersList.vue';

import ItemsNew from '../views/ItemsNew.vue';
import ItemsEdit from '../views/ItemsEdit.vue';
import ItemsList from '../views/ItemsList.vue';

import UsersNew from '../views/UsersNew.vue';
import UsersList from '../views/UsersList.vue';

import CashierLogin from '../views/cashier/Login.vue';
import CashierBasket from '../views/cashier/Basket.vue';
import CashierItemsList from '../views/cashier/ItemsList.vue';
import CashierConfirmCharge from '../views/cashier/ConfirmCharge.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      component: FrontPage,
    },
    {
      path: '/login',
      component: Login,
    },
    {
      path: '/reset-password',
      component: ResetPassword,
    },
    {
      path: '/dashboard',
      component: Dashboard,
    },
    {
      path: '/permissions',
      component: Permissions,
    },
    {
      path: '/items',
      component: ItemsList,
    },
    {
      path: '/items/new',
      component: ItemsNew,
    },
    {
      path: '/items/edit',
      component: ItemsEdit,
    },
    {
      path: '/users',
      component: UsersList,
    },
    {
      path: '/users/new',
      component: UsersNew,
    },
    {
      path: '/customers',
      component: CustomersList,
    },
    {
      path: '/customers/new',
      component: CustomersNew,
    },
    {
      path: '/cashier',
      component: CashierLogin,
    },
    {
      path: '/cashier/basket',
      component: CashierBasket,
    },
    {
      path: '/cashier/items',
      component: CashierItemsList,
    },
    {
      path: '/cashier/charge',
      component: CashierConfirmCharge,
    },
  ],
});

export default router;
