import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faUserSecret,
  faSquarePollVertical,
  faTag,
  faChartLine,
  faUserGroup,
  faClipboardList,
  faList,
  faSquareCheck,
  faMagnifyingGlass,
  faWallet,
  faDollarSign,
  faCircleXmark,
  faFloppyDisk,
  faPercent,
  faDollar,
  faTrashCan,
  faEllipsisVertical,
  faArrowDownAZ,
  faChevronLeft,
  faChevronRight,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
// Then add it to library

library.add(
  faUserSecret,
  faSquarePollVertical,
  faTag,
  faChartLine,
  faUserGroup,
  faClipboardList,
  faList,
  faSquareCheck,
  faMagnifyingGlass,
  faWallet,
  faDollarSign,
  faCircleXmark,
  faFloppyDisk,
  faPercent,
  faDollar,
  faTrashCan,
  faEllipsisVertical,
  faArrowDownAZ,
  faChevronLeft,
  faChevronRight
);

const app = createApp(App);

app.component('font-awesome-icon', FontAwesomeIcon);

app.use(router);

app.mount('#app');
